#!/usr/bin/env node

const process = require("process");
const lib = require("../../lib/v0.0.1");
const args = require("args");
const path = require("path")


args.parse(process.argv);
console.log(args.sub);
if (args.sub.length == 0) {
    lib.speed_test("", process.cwd())
} else {
    let filename = args.sub[0];
    lib.speed_test(path.join(process.cwd(), filename), process.cwd())
}