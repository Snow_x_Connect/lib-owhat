#!/usr/bin/env node

const process = require("process");
const fs = require("fs");
const path = require("path");
const glob = require("glob");
const args = require("args");
const lodash = require("lodash");
const events = require("events");
const parallelLimit = require('run-parallel-limit');
const OwhatQiniuWrapper = require("../../lib/v0.0.1").OwhatWrapper
const Toolbox = require("../../lib/v0.0.1/Toolbox");

let cwd = process.cwd();
console.log(cwd);
args.option("max", "Max paralell uploads", 5)
  .option("size", "Single file size limit , default to split 1024MB", 1024)
  .option("fileonly", "only upload files,do NOT upload dirs", false)
  .option("dironly", "only upload dirs,do NOT upload files", false);
let flags = args.parse(process.argv);
if (flags.d && flags.f) {
  console.error("`file_only` and `dir_only` can NOT be true in the same time!");
  process.exit(1);
}
let sub = args.sub;

OwhatQiniuWrapper.getValidInstance("hujimiya@ya.ru", "owhat65huji").then(async o_instance => {
  if (!o_instance.ok) {
    console.error(o_instance.msg);
    process.exit(1);
  }
  let owhat = o_instance.instance;
  let paths_to_upload = await getPaths();
  /**@type {Number} */
  let max_upload = Number(flags.m);
  let bytes_limit = parseInt(flags.s) * 1024 * 1024;
  let tasks = paths_to_upload.map(full_path => async function task(cb) {
    let stats = (await Toolbox.getStats(full_path)).stats;
    if (stats.isFile()) {
      let o_upload = await owhat.uploadFileByMkblk(full_path, bytes_limit);
      if (!o_upload.ok) {
        console.error("[ ERROR ]", full_path, ":", o_upload.msg);
      } else {
        console.log(o_upload.files.map(f => {
          return `${f.name}\n${f.url}`
        }).join("\n------------------------------------------------------\n"));
        console.log("");
      }
    } else {
      let o_upload = await owhat.safelyUploadDirByMkblk(full_path, bytes_limit);
      console.log("[DIR]:", full_path)
      console.log(o_upload.errors.join("\n!!!XXX!!!XXX!!!XXX!!!\n"));
      console.log(o_upload.files.map(f => {
        if(!f.chunks){
          debugger
        }
        if (f.chunks.length == 1) {
          return `${f.path}\n${f.chunks[0].url}`
        } else {
          return `${f.path}\n${f.chunks.map(c=>`${c.name}\n${c.url}`).join("\n")}`
        }
      }).join("\n-------==------==-------\n"))
    }
    cb(null, {});
  });
  parallelLimit(tasks, max_upload, (err, results) => {
    console.log("\n\n↓ ---tasks done...");
    process.exit(0)
  })


});


/**
 * @return {Promise<string[]>}
 */
function getPaths() {
  return new Promise(async resolve => {
    let find_files_by_glob = sub.map(p => new Promise((resolve, reject) => {
      glob(p, (err, matches) => {
        if (err) {
          return reject(err);
        } else {
          resolve(matches);
        }
      })
    }));
    let find_files_by_raw = sub.map(p => new Promise((resolve, reject) => {
      if (path.isAbsolute(p)) {
        resolve([path.resolve(p)]);
      } else {
        resolve([path.resolve(cwd, p)])
      }
    }));
    Promise.all([...find_files_by_glob, ...find_files_by_raw]).then(async ([...matches]) => {
      let ms = lodash.union(...matches.map(e => e.map(ele => path.resolve(cwd, ele))));
      ms = await asyncFilterMs(ms);
      resolve(ms);
    })


  })
};

/**
 *
 *@returns {Promise<Boolean>}
 * @param {String} full_path
 */
function checkOk(full_path) {
  return new Promise(async resolve => {
    let o_stats = await Toolbox.getStats(full_path);
    if (!o_stats.ok) {
      return resolve(false);
    }
    if (flags.f) {
      return resolve(o_stats.stats.isFile());
    }
    if (flags.d) {
      return resolve(o_stats.stats.isDirectory());
    }
    return resolve(
      o_stats.stats.isDirectory() || o_stats.stats.isFile()
    );
  })
};

/**
 * @returns {Promise<String[]>}
 * @param {String[]} ms 
 */
function asyncFilterMs(ms) {
  return new Promise(async resolve => {
    let checks = ms.map(p => checkOk(p));
    let okms = [];
    let o_checks = await Promise.all(checks);
    o_checks.forEach((v, index) => {
      if (v) {
        okms.push(ms[index]);
      }
    });
    resolve(okms);
  })
}